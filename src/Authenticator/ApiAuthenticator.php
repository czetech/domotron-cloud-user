<?php

namespace DomotronCloudUser\Authenticator;

use DomotronCloudClient\CloudClient;
use DomotronCloudClient\Exception\CloudClientException;
use DomotronCloudUser\Exception\ExternalCallException;

class ApiAuthenticator implements IAuthenticator
{
    /** @var CloudClient */
    private $client;

    /**
     * @param CloudClient $client
     */
    public function __construct(CloudClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $username
     * @param string|null $password
     * @return array [$sessionId, $expire]
     */
    public function authenticate($username, $password = null)
    {
        return [false, false];
    }

    /**
     * @param string|null $sessionId
     * @return bool
     * @throws ExternalCallException
     */
    public function invalidate($sessionId = null)
    {
        if (!$sessionId) {
            return true;
        }

        try {
            return $this->client->userEndpoint()->invalidateSession($sessionId);
        } catch (CloudClientException $e) {
            throw new ExternalCallException();
        }
    }

    /**
     * @param string $sessionId
     * @return int expiration date
     * @throws ExternalCallException
     */
    public function extend($sessionId)
    {
        try {
            return $this->client->userEndpoint()->extendSession($sessionId);
        } catch (CloudClientException $e) {
            throw new ExternalCallException();
        }
    }
}
