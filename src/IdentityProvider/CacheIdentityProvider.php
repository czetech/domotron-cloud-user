<?php

namespace DomotronCloudUser\IdentityProvider;

use DomotronCloudUser\Exception\ExternalCallException;
use DomotronCloudUser\IdentityProvider\Cache\ICache;
use DomotronCloudUser\Identity;

abstract class CacheIdentityProvider implements IIdentityProvider
{
    /** @var ICache */
    protected $cache;

    /**
     * @param ICache $cache
     */
    public function __construct(ICache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Fetch token data
     * @param string $cookieName
     * @return Identity
     * @throws ExternalCallException
     */
    public function getIdentity($cookieName)
    {
        // If cookie is not set (or is expired) no data are available
        $sessionId = filter_input(INPUT_COOKIE, $cookieName);
        if (!$sessionId) {
            return new Identity();
        }

        $cachedData = $this->cache->get();
        if ($cachedData) {
            return new Identity($cachedData);
        }

        $userData = $this->fetchIdentity($sessionId);
        if ($userData === null) {
            return new Identity();
        }

        $this->cache->set($userData);
        return new Identity($userData, true);
    }

    /**
     * Clear all identity data from cache
     * @return bool
     */
    public function clearCache()
    {
        return $this->cache->clear();
    }

    /**
     * Fetch data from external source
     * @param string $sessionId
     * @return array|null
     */
    abstract protected function fetchIdentity($sessionId);
}
