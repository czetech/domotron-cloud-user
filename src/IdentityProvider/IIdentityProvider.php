<?php

namespace DomotronCloudUser\IdentityProvider;

use DomotronCloudUser\Identity;

interface IIdentityProvider
{
    /**
     * Fetch token data
     * @param string $cookieName
     * @return Identity
     */
    public function getIdentity($cookieName);

    /**
     * Clear all identity data from cache
     * @return bool
     */
    public function clearCache();
}
