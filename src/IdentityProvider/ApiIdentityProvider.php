<?php

namespace DomotronCloudUser\IdentityProvider;

use DomotronCloudClient\CloudClient;
use DomotronCloudClient\Exception\CloudClientException;
use DomotronCloudUser\Exception\ExternalCallException;
use DomotronCloudUser\IdentityProvider\Cache\ICache;

class ApiIdentityProvider extends CacheIdentityProvider
{
    /** @var CloudClient */
    protected $cloudClient;

    /** @var ICache */
    protected $cache;

    /**
     * @param CloudClient $cloudClient
     * @param ICache $cache
     */
    public function __construct(
        CloudClient $cloudClient,
        ICache $cache
    ) {
        parent::__construct($cache);
        $this->cloudClient = $cloudClient;
    }

    /**
     * Fetch data from external source
     * @param string $sessionId
     * @return array
     * @throws ExternalCallException
     */
    public function fetchIdentity($sessionId)
    {
        try {
            $userId = $this->cloudClient->userEndpoint()->getUserIdFromSession($sessionId);
            if (!$userId) {
                return null;
            }

            $userData = $this->cloudClient->userEndpoint()->getUserData($userId);
            if (!$userData) {
                return null;
            }
        } catch (CloudClientException $e) {
            throw new ExternalCallException();
        }

        return $userData;
    }
}
