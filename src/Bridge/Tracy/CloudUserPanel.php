<?php

namespace DomotronCloudUser\Bridge\Tracy;

use DomotronCloudUser\CloudUser;
use Tracy\IBarPanel;

class CloudUserPanel implements IBarPanel
{
    /** @var CloudUser */
    private $cloudUser;

    /**
     * @param CloudUser $cloudUser
     */
    public function __construct(CloudUser $cloudUser)
    {
        $this->cloudUser = $cloudUser;
    }

    /**
     * Renders tab.
     * @return string
     */
    public function getTab()
    {
        if (headers_sent() && !session_id()) {
            return;
        }

        ob_start();
        $cloudUser = $this->cloudUser;
        require __DIR__ . '/templates/CloudUserPanel.tab.phtml';
        return ob_get_clean();
    }

    /**
     * Renders panel.
     * @return string
     */
    public function getPanel()
    {
        ob_start();
        $cloudUser = $this->cloudUser;
        require __DIR__ . '/templates/CloudUserPanel.panel.phtml';
        return ob_get_clean();
    }
}
