<?php

namespace DomotronCloudUser\Permissions;

use DomotronCloudUser\Exception\ExternalCallException;
use DomotronCloudUser\Permissions\Cache\ICache;
use DomotronCloudUser\Permissions\Driver\IDriver;
use Exception;

class PermissionsList
{
    /** @var IDriver */
    private $driver;

    /** @var ICache */
    private $cache;

    /** @var array */
    private $keyAliases;

    /** @var int */
    private $cloudUserId;

    /**
     * PermissionsList constructor.
     * @param IDriver $driver
     * @param ICache $cache
     * @param array $keyAliases
     * @param $cloudUserId
     */
    public function __construct(IDriver $driver, ICache $cache, array $keyAliases, $cloudUserId)
    {
        $this->driver = $driver;
        $this->cache = $cache;
        $this->cloudUserId = $cloudUserId;
        $this->keyAliases = $keyAliases;
    }

    /**
     * @param array $keys
     * @return bool
     * @throws ExternalCallException
     */
    public function prepareMulti(array $keys)
    {
        $toFetch = [];

        foreach ($keys as $key) {
            $parsedKey = $this->parseKey($key);
            $permissions = $this->cache->get($parsedKey);
            if (!$permissions) {
                $toFetch[] = $parsedKey;
            }
        }

        if (!count($toFetch)) {
            return true;
        }

        try {
            $permissions = $this->driver->getPermissions($this->cloudUserId, $toFetch);
        } catch (Exception $e) {
            throw new ExternalCallException();
        }

        foreach ($permissions as $permKey => $permission) {
            $this->cache->set($permKey, new Permissions($permission));
        }

        return true;
    }

    /**
     * @param string $key
     * @return Permissions|null
     * @throws ExternalCallException
     */
    public function get($key)
    {
        $parsedKey = $this->parseKey($key);
        $permissions = $this->cache->get($parsedKey);
        if ($permissions) {
            return $permissions;
        }

        try {
            $permissions = $this->driver->getPermissions($this->cloudUserId, [$parsedKey]);
        } catch (Exception $e) {
            throw new ExternalCallException();
        }

        $this->cache->set($parsedKey, new Permissions($permissions[$parsedKey]));
        return $this->cache->get($parsedKey);
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->cache->getAll();
    }

    /**
     * @param string $key
     * @return string
     */
    private function parseKey($key)
    {
        if (substr($key, 0, 1) === '%') {
            return isset($this->keyAliases[substr($key, 1)]) ? $this->keyAliases[substr($key, 1)] : $key;
        }
        return $key;
    }
}
