<?php

namespace DomotronCloudUser;

use ArrayAccess;
use Exception;

class Identity implements ArrayAccess
{
    /** @var array */
    private $data;

    /** @var bool */
    private $isFresh;

    /**
     * @param array $data
     * @param bool $isFresh
     */
    public function __construct(array $data = [], $isFresh = false)
    {
        $this->data = $data;
        $this->isFresh = (bool) $isFresh;
    }

    /**
     * @return bool
     */
    public function isFresh()
    {
        return $this->isFresh;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return !(bool) count($this->data);
    }

    /*****************************************************************
     * Data access methods
     *****************************************************************/

    /**
     * Returns all user data
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return isset($this->data['id']) ? $this->data['id'] : null;
    }

    /**
     * @return int|null
     */
    public function getUsername()
    {
        return isset($this->data['username']) ? $this->data['username'] : null;
    }

    /**
     * @return int|null
     */
    public function getName()
    {
        return isset($this->data['name']) ? $this->data['name'] : null;
    }

    /**
     * @return bool|null
     */
    public function isDemo()
    {
        return isset($this->data['is_demo']) ? $this->data['is_demo'] : null;
    }

    /**
     * @return string|null
     */
    public function getLanguage()
    {
        return isset($this->data['language']) ? $this->data['language'] : null;
    }

    /**
     * @return array|null
     */
    public function getFavoriteProject()
    {
        if (!isset($this->data['favorite']['project'])) {
            return null;
        }

        if (empty($this->data['favorite']['project'])) {
            return null;
        }

        return $this->data['favorite']['project'];
    }

    /*****************************************************************
     * Array access implementation
     *****************************************************************/

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @throws Exception
     */
    public function offsetSet($offset, $value)
    {
        throw new Exception('Identity is immutable');
    }

    /**
     * @param mixed $offset
     * @throws Exception
     */
    public function offsetUnset($offset)
    {
        throw new Exception('Identity is immutable');
    }
}
