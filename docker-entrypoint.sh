#!/bin/sh
set -e

case $1 in

  *)
    exec "$@"
  ;;

esac

exit 0
