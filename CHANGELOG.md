# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.0] - 2021-02-27

## [1.4.0] - 2019-03-29

### Changed

- metóda permissionsIsAllowed rozšírená o default parameter použitý ak je cloud user neaktívny

## [1.3.0] - 2018-10-09

### Changed

- metóda getFavoritePld() na getFavoriteProject()

## [1.2.1] - 2018-09-11

### Fixed

- Wrapper pre favorite control zmeneny na favorite plc

## [1.2.0] - 2018-09-10

### Added

- Pridany wrapper pre favorite control do Identity

## [1.1.0] - 2018-08-17

### Added

- Možnosť aliasovať kľúče v PermissionsListFactory

## [1.0.1] - 2018-08-16

### Changed

- Upravene spracovanie flagu isActive

## [1.0.0] - 2018-08-09

### Changed

- Použitá nová verzia libky cloudClient

## [0.3.0] - 2018-08-09

### Added

- Pridaná metóda getCookieTokenValue() do CloudClient-a

## [0.2.0] - 2018-07-10

### Changed

- Upravená použitá verzia cloud-client libky

## [0.1.0] - 2018-06-20

### Added

- Initial version

[Unreleased]: https://gitlab.com/domotron/cloud-user/compare/1.5.0...master
[1.5.0]: https://gitlab.com/domotron/cloud-user/compare/1.4.0...1.5.0
[1.4.0]: https://gitlab.com/domotron/cloud-user/compare/1.3.0...1.4.0
[1.3.0]: https://gitlab.com/domotron/cloud-user/compare/1.2.1...1.3.0
[1.2.1]: https://gitlab.com/domotron/cloud-user/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/domotron/cloud-user/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/domotron/cloud-user/compare/1.0.1...1.1.0
[1.0.1]: https://gitlab.com/domotron/cloud-user/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/domotron/cloud-user/compare/0.3.0...1.0.0
[0.3.0]: https://gitlab.com/domotron/cloud-user/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/domotron/cloud-user/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/domotron/cloud-user/tree/0.1.0
